# [Corona] TableView

A tiny demo application for TableView.

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/CoronaTableView.git
```

Alternatively, download the compressed repo as a zip file.

Then, open *main.lua* with [Corona](https://coronalabs.com/product/).

## Copyright

2018, Michael Chen; Apache 2.0