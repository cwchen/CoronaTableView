-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

-- Init ranks with different weight.
local ranks = {}

local weight = 1

for i = 1, 15 do
  ranks[weight] = "shabby"
  weight = weight + 1
end

for i = 1, 50 do
  ranks[weight] = "common"
  weight = weight + 1
end

for i = 1, 10 do
  ranks[weight] = "uncommon"
  weight = weight + 1
end

for i = 1, 5 do
  ranks[weight] = "rare"
  weight = weight + 1
end

for i = 1, 2 do
  ranks[weight] = "epic"
  weight = weight + 1
end

ranks[weight] = "legendary"

local weapons = {
  "gloves",
  "dagger",
  "sword",
  "great sword",
  "mace",
  "great mace",
  "axe",
  "great axe",
  "polearms",
}

-- Init a seed by system time.
math.randomseed(os.time())

-- Create a random list of weapons.
local weaponLists

local function reloadList ()
  weaponLists = {}

  for i = 1, 10 do
    iRank = math.random(1, #ranks)
    iWeapon = math.random(1, #weapons)

    weaponLists[i] = ranks[iRank] .. " " .. weapons[iWeapon]
  end
end

-- Init the list.
reloadList()

local text = display.newText(
  {
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.1,
    text = "Weapon Shop",
    fontSize = 28,
  }
)

text:setFillColor(0, 0, 0)

local function onRowRender (event)
  local row = event.row

  -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
  local rowHeight = row.contentHeight
  local rowWidth = row.contentWidth

  local rowTitle = display.newText( row, weaponLists[row.index], 0, 0, nil, 17 )
  rowTitle:setFillColor( 0 )

  -- Align the label left and vertically centered
  rowTitle.anchorX = 30
  rowTitle.x = rowWidth * 0.8
  rowTitle.y = rowHeight * 0.5
end

local tableView = widget.newTableView(
  {
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.5,
    width = display.contentWidth * 0.9,
    height = display.contentHeight * 0.5,
    onRowRender = onRowRender,
  }
)

for i = 1, 10 do
  tableView:insertRow({})
end

local function btnListener (event)
  if event.phase == "ended" then
    reloadList()
    tableView:reloadData()
  end
end

local button = widget.newButton(
  {
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.9,
    label = "Reload",
    isEnabled = true,
    onEvent = btnListener,
    shape = "rounedRect",
    labelColor = {
      default = { 0 / 255, 0 / 255, 0 / 255},
      over = { 0 / 255, 0 / 255, 0 / 255},
    },
    fillColor = {
      default = { 180 / 255, 255 / 255, 255 / 255},
      over = { 110 / 255, 255 / 255, 255 / 255},
    }
  }
)
